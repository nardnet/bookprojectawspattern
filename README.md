# Building-and-Delivering-Microservices-on-AWS

## Chapter 1 : Software Architecture Patterns
In this chapter, you will learn about software architecture and what makes software architecture. You will learn about software architecture patterns and understand different major software architecture patterns that exist to develop software. After reading this chapter you will have a solid understanding of Layered architecture, Microkernel Archichitecture, Pipeline Architecture, Service Oriented Architecture, Event Driven Architecture, Microservice architecture and other few major architecture patterns. This chapter will discuss the real word examples for each of the architecture patterns.

## Chapter 2 : Microservices Fundamentals and Design Patterns
This chapter describes what microservice is and how this can be useful to solve some of the software delivery challenges. This chapter explains different strategies and design patterns to break a monolithic application into a microservice. After reading this chapter you will get a fair understanding of microservices and what challenges that brings to the table. In this chapter, you will learn what a monolithic and microservice architecture really means. I will be explaining the different microservice patterns which can help you to design good manageable microservices.

## Chapter 3 : CI/CD Principles and Microservice Development
In this chapter we create a sample java spring boot application to be deployed as a microservices and expose a rest endpoint to ensure that our users are able to access this endpoint.This chapter explains the different tools and technologies used to develop the application and provide the sample code to users.

### Code action
#### aws-code-pipeline directory contains the source code for sample microservice.

How to build source code
Use mvn clean install to build the source code. Use java -j target/aws-code-pipeline*.jar to run the spring boot application

## Chapter 4 : CI/CD Principles and Microservice Development
This chapter explains what infrastructure as a code and what tools and technologies you can use to provision different resources you require to deploy the sample application in AWS cloud. For this book we will be creating infrastructure using the AWS console, but in this chapter we will explain what all tools are available to you in order to create these resources.

### Code action
#### How to use  code
CF_template.json file contains the cloud formation template used in the chapter. Terraform directory conatins the terraform template to create the infrastructure.
* Use `terraform init` to initialize the terraform template.
* Use `terraform plan` to see what all resources will be created using terraform.
* Use `terraform apply --auto-approve` to create the terraform resources in your AWS account.
* Use `terraform destroy --auto-approve` to delete the resources managed by the terraform in your AWS account.